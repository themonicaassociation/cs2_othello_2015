#include "player.h"
#include <cstdio>
#include <iostream>

/*
 * this is a nice looking comment by dryden
 * and this is a better one by edward!
 */
 
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    board = Board(); // ??
    other = WHITE;
    if (side == WHITE)
    {
      other = BLACK;
    }
  
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    if (opponentsMove != NULL)
    {
        board.doMove(opponentsMove, other);
    }
    
    if (board.countBlack() == 2 && board.countWhite() == 2)
    {
	side = BLACK;
	other = WHITE;
      }
    if (board.isDone())
    {
      return NULL;
    }

    Move *m = NULL;
    if (board.hasMoves(side))
      {
	for (int i = 0; i < 8; i++)
	  {
	    for (int j = 0; j < 8; j++)
	      {
		m = new Move(i, j);
		if (board.checkMove(m, side))
		  {
		    board.doMove(m, side);
		    return m;
		  }
		delete m;
	      }
	  }
      }
    return NULL;
}

/* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
